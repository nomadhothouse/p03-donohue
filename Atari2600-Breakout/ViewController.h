//
//  ViewController.h
//  Atari2600-Breakout
//
//  Created by Thomas Donohue on 2/9/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameField.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *livesLabel;

@property (strong, nonatomic) IBOutlet GameField *gameField;


@end

