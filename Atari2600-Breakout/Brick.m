//
//  Brick.m
//  Atari2600-Breakout
//
//  Created by Thomas Donohue on 2/11/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import "Brick.h"

@implementation Brick

- (void) initWithParams:(NSInteger)xPos y:(NSInteger)yPos width:(NSInteger)brickWidth height:(NSInteger)brickHeight
    value:(NSInteger) brickVal
{
    //self = [super init];
    if(self) {
        NSLog(@"_init: %@", self);
        self.x = xPos;
        self.y = yPos;
        self.width = brickWidth;
        self.height = brickHeight;
        self.value = brickVal;
    }
    //return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
/**- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGRectMake(self.x, self.y, self.width, self.height);

}**/




@end
