//
//  Brick.h
//  Atari2600-Breakout
//
//  Created by Thomas Donohue on 2/11/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Brick : UIView

@property (nonatomic, assign) NSInteger x;
@property (nonatomic, assign) NSInteger y;
@property (nonatomic, assign) NSInteger width;
@property (nonatomic, assign) NSInteger height;
@property (nonatomic, assign) NSInteger value;

- (void) initWithParams:(NSInteger)xPos y:(NSInteger)yPos width:(NSInteger)brickWidth height:(NSInteger)brickHeight;

@end
