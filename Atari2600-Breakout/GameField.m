//
//  GameField.m
//  Atari2600-Breakout
//
//  Created by Thomas Donohue on 2/10/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import "GameField.h"

@implementation GameField

@synthesize paddle, ball;
@synthesize timer;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

static const NSInteger START_Y = 100;
static const NSInteger BRICK_HEIGHT = 14;

static const NSInteger PADDLE_INIT_WIDTH = 75;
static const NSInteger PADDLE_HEIGHT = 14;

static const NSInteger NUM_BRICKS_IN_ROW = 14;
static const NSInteger NUM_ROWS = 6;

static const NSInteger BALL_SIZE = 10;

static const BOOL _DEBUG = NO;


static CGFloat timerSpeed = .025;
static NSInteger ballSpeedX = 10;
static NSInteger ballSpeedY = 10;

static UILabel *scoreLbl;
static UILabel *livesLbl;

static BOOL runningGame = YES;

CGFloat ballSpin = 1.0;

//========================================LOADING================================================

-(void) initField: (UILabel*) scoreLabel label2: (UILabel*) livesLabel
{
    scoreLbl = scoreLabel;
    livesLbl = livesLabel;
    self.score = 0;
    self.lives = 3;
    [self updateScore];
    [self updateLives];
    
    [self layoutIfNeeded];
    bounds = [self bounds];
    
    if(_DEBUG){
        NSLog(@"initField");
        NSLog(@"width: %f\n", bounds.size.width);
        NSLog(@"height: %f\n", bounds.size.height);
    }
    
    UIColor *pColor = [UIColor colorWithRed:200.0f/255.0f green:72.0f/255.0f blue:72.0f/255.0f alpha:1.0];
    
    paddleX = bounds.size.width / 2;
    paddleY = 6 * bounds.size.height / 7;
    
    paddleWidth = PADDLE_INIT_WIDTH;
    
    
    paddle = [[UIView alloc] initWithFrame:CGRectMake(paddleX, paddleY, paddleWidth, PADDLE_HEIGHT)];
    
    paddle.backgroundColor = pColor;
    
    CGFloat ballX = bounds.size.width / 4;
    CGFloat ballY = paddleY - bounds.size.height / 7;
    
    
    ball = [[UIView alloc] initWithFrame:CGRectMake(ballX, ballY, BALL_SIZE, BALL_SIZE)];
    
    ball.backgroundColor = pColor;
    
    dx = ballSpeedX;
    dy = -ballSpeedY;
    
    [self addSubview: paddle];
    [self addSubview: ball];
    [self startAnimation];
    
}

- (void) resetBricks
{
    
    for (int i = 0; i < NUM_ROWS; i++) {
        for (int j = 0; j < NUM_BRICKS_IN_ROW; j++) {
            [brickArr[i][j] setHidden:NO];
        }
    }
    //[self initBricks];
}

- (void) initBricks
{
    bounds = [self bounds];
    if(_DEBUG){
        NSLog(@"width: %f\n", bounds.size.width);
        NSLog(@"height: %f\n", bounds.size.height);
    }
    paddle.center = CGPointMake(paddleX, paddleY);
    NSInteger startY = START_Y;
    NSInteger brickWidth = bounds.size.width / NUM_BRICKS_IN_ROW;
    NSInteger brickHeight = BRICK_HEIGHT;
    NSInteger currentX, currentY;
    
    brickArr = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < NUM_ROWS; i++) {
        currentY = startY + i * brickHeight;
        brickArr[i] = [[NSMutableArray alloc] init];
        for (int j = 0; j < NUM_BRICKS_IN_ROW; j++) {
            currentX = j * brickWidth;
            
            UIView *brick = [[UIView alloc] initWithFrame:CGRectMake(currentX, currentY, brickWidth, brickHeight)];

            UIColor *brickColor = [self getRowColor: i];
            
            brick.backgroundColor = brickColor;
            
            [brickArr[i] addObject: brick];
            [self addSubview: brick];
        }
    }
    

    
}

//======================================================================================================

-(id) intersectsBrick: (UIView*) theBall
{
    for (int i = 0; i < NUM_ROWS; i++) {
        for (int j = 0; j < NUM_BRICKS_IN_ROW; j++) {
            if ([brickArr[i][j] isHidden] == NO && CGRectIntersectsRect([theBall frame], [brickArr[i][j] frame])){
                self.score += [self getRowScore: i];
                [self updateScore];
                return brickArr[i][j];
            }
        }
    }
    return nil;
}

//========================================TOUCH HANDLERS================================================

// http://stackoverflow.com/questions/14856906/touchesbegan-touchesended-touchesmoved-for-moving-uiview

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_DEBUG) { NSLog(@"touchesBegan"); }
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];
    for (UITouch *t in touches)
    {
        CGPoint p = [t locationInView:self];
        if(CGRectContainsPoint([paddle frame], p)){
            dragging = YES;
            oldX = touchLocation.x;
            //oldY = touchLocation.y;
        }
    }
    if(!runningGame){
        runningGame = !runningGame;
        [ball setHidden: NO];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_DEBUG) { NSLog(@"touchesEnded"); }
    dragging = NO;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_DEBUG){ NSLog(@"touchesMoved"); }
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];
    CGPoint prevLocation= [touch previousLocationInView: touch.view];
        if (dragging) {
            dragVelocity = touchLocation.x - prevLocation.x;
            if(_DEBUG) { NSLog(@"dragging"); }
            CGRect frame = paddle.frame;
            frame.origin.x = paddle.frame.origin.x + touchLocation.x - oldX;
            //NSLog(@"%f\n", frame.origin.x);
            //NSLog(@"%f\n",paddle.center.x - paddleWidth / 2);
            //NSLog(@"%f\n",paddle.center.x + paddleWidth / 2);
            if(frame.origin.x > 0 && frame.origin.x + paddleWidth < bounds.size.width){
                paddle.frame = frame;
            }
        }else{
            dragVelocity = 0;
        }
        //NSLog(@"%f\n", touchLocation.x - prevLocation.x);
}


//========================================TIMER===================================================

-(IBAction) startAnimation
{
    timer = [NSTimer scheduledTimerWithTimeInterval:timerSpeed
                                             target:self
                                             selector:@selector(timerEvent:)
                                             userInfo:nil
                                             repeats:YES];
    
}

-(IBAction) stopAnimation
{
    [timer invalidate];
}

-(void) timerEvent:(id)sender
{
    
    CGPoint ballP = [ball center];
    BOOL lostLife = NO;
    if(runningGame){
        if ((ballP.x + dx) < 0){
            dx = -dx;
        }

        if ((ballP.y + dy) < 0){
            dy = -dy;

        }

        if ((ballP.x + dx) > bounds.size.width){
            dx = -dx;
        }

        if ((ballP.y + dy) > bounds.size.height){
            lostLife = YES;
            self.lives--;
            if(self.lives == 0){
                NSString *msg = [NSString stringWithFormat:@"Score: %ld", (long)self.score];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Game Over"
                                                                message: msg
                                                               delegate:nil
                                                      cancelButtonTitle:@"Play Again"
                                                      otherButtonTitles:nil];
                [alert show];
                self.lives = 3;
                self.score = 0;
                [self updateScore];
                [self resetBricks];
            }
            [self updateLives];
            runningGame = NO;
            CGFloat ballX = bounds.size.width / 4;
            CGFloat ballY = paddleY - bounds.size.height / 7;
            paddleX = bounds.size.width / 2;
            [paddle setCenter:CGPointMake(paddleX, paddleY)];
            
            
            [ball setCenter:CGPointMake(ballX, ballY)];
            [ball setHidden: YES];
            
            dx = ballSpeedX;
            dy = -ballSpeedY;
        }

        if(!lostLife){
            ballP.x += dx;
            ballP.y += dy;
            [ball setCenter: ballP];
        }

        UIView *checkBrick = [self intersectsBrick: ball];
        if(checkBrick != nil){
            [checkBrick setHidden: YES];
            [self reflectBall];
        } else if (CGRectIntersectsRect([ball frame], [paddle frame])) {
            [self reflectBallOffPaddle];
        }
    }
    
    
}

//========================================UTILITIES================================================
- (void) updateScore
{
    scoreLbl.text = [NSString stringWithFormat:@"%ld", (long)self.score];
}
- (void) updateLives
{
    livesLbl.text = [NSString stringWithFormat:@"%ld", (long)self.lives];
}

- (void) reflectBall
{
    CGPoint ballP = [ball center];
    dy = -dy;
    ballP.y += 2 * dy;
    [ball setCenter: ballP];
}

- (void) reflectBallOffPaddle
{
    CGPoint ballP = [ball center];
    CGPoint paddleP = [paddle center];
    //NSLog(@"ball center x: %f\n", ballP.x);
    //NSLog(@"paddle center x: %f\n", paddleP.x);
    //NSLog(@"%f\n", ballP.x - paddleP.x);
    CGFloat diff = ball.frame.origin.x - paddle.frame.origin.x;
    double distance = [self getDistance:ballP point2:paddleP];
    

    //NSLog(@"%f\n", distance);
    
    /**CGFloat firstBound = paddleP.x - (paddleWidth / 2);
    CGFloat secondBound = paddleP.x - (paddleWidth / 4);
    CGFloat thirdBound = paddleP.x;
    CGFloat fourthBound = paddleP.x + (paddleWidth / 4);
    CGFloat fifthBound = paddleP.x + (paddleWidth / 2);**/
    
    //if(ballP.x >
    //NSLog(@"ball frame origin x: %f\n", ball.frame.origin.x);
    //NSLog(@"paddle frame origin x: %f\n", paddle.frame.origin.x);
    
    /**CGFloat xFactor = 0, yFactor = 0;
    
    if(dx > 0){
        xFactor = 1;
    }else{
        xFactor = -1;
    }
    
    if(dy > 0){
        yFactor = 1;
    }else{
        yFactor = -1;
    }
    
    CGFloat vParallel = dx + dy;
    CGFloat vNormal = dx - dy;
    CGFloat radius = BALL_SIZE / 2;
    
    CGFloat cBefore = radius * ballSpin;
    CGFloat vBefore = vParallel;
    
    CGFloat cAfter = (-1.0/3.0) * cBefore + (-4.0/3.0) * vBefore + (4.0/3.0) * ballSpeed;
    CGFloat vAfter = (-2.0/3.0) * cBefore + ( 1.0/3.0) * vBefore + (2.0/3.0) * ballSpeed;
    
    CGFloat vNAfter = -vNormal;
    
    CGFloat vHAfter = xFactor * vAfter  - yFactor * vNAfter;
    CGFloat vVAfter = xFactor * vNAfter + yFactor * vAfter;**/
    
    /**if((dy > 0 && vHAfter > 0) || (dy < 0 && vHAfter < 0)){
        vVAfter = -vHAfter;
    }**/
    
    
    if(diff > 0 && diff < paddleWidth && ball.frame.origin.y < paddle.frame.origin.y){
        /**
         Vector2 collisionNormal = Vector2.Normalize(paddleNormal + (paddleVelocity * desiredEffectAmount));
         ballVelocity = Vector2.Reflect(ballVelocity, collisionNormal);
         **/
        
        
        /**dx = vHAfter * 0.5;
        dy = vVAfter * 0.5;
        ballSpin = cAfter / radius;**/
        dy = -dy;
        //dx = 0;
        //dy = -ballSpeedY + (distance * 0.25);
        //dx = ballSpeedX + (distance * 0.25);
        //dx = 1.5 * dx;
        ballP.y += 2 * dy;
        [ball setCenter: ballP];
    }
}

- (double) getDistance: (CGPoint) p1 point2:(CGPoint) p2
{
    double xd = (p2.x - p1.x);
    //double yd = (p2.y - p1.y);
    double dist = sqrt(xd*xd);
    return dist;
}

- (NSInteger) getRowScore: (NSInteger) row
{
    NSInteger rowScore = 0;
    switch(row){
        case 0:
            rowScore += 7;
            break;
        case 1:
            rowScore += 7;
            break;
        case 2:
            rowScore += 4;
            break;
        case 3:
            rowScore += 4;
            break;
        case 4:
            rowScore += 1;
            break;
        case 5:
            rowScore += 1;
            break;
            
    }
    return rowScore;
}

- (UIColor *) getRowColor: (NSInteger) row
{
    UIColor *rowColor = nil;
    switch(row){
        case 0:
            rowColor = [UIColor colorWithRed:200.0f/255.0f green:72.0f/255.0f blue:72.0f/255.0f alpha:1.0];
            break;
        case 1:
            rowColor = [UIColor colorWithRed:198.0f/255.0f green:108.0f/255.0f blue:58.0f/255.0f alpha:1.0];
            break;
        case 2:
            rowColor = [UIColor colorWithRed:180.0f/255.0f green:122.0f/255.0f blue:48.0f/255.0f alpha:1.0];
            break;
        case 3:
            rowColor = [UIColor colorWithRed:162.0f/255.0f green:162.0f/255.0f blue:42.0f/255.0f alpha:1.0];
            break;
        case 4:
            rowColor = [UIColor colorWithRed:72.0f/255.0f green:160.0f/255.0f blue:72.0f/255.0f alpha:1.0];
            break;
        case 5:
            rowColor = [UIColor colorWithRed:66.0f/255.0f green:72.0f/255.0f blue:200.0f/255.0f alpha:1.0];
            break;
        default:
            rowColor = [UIColor colorWithRed:200.0f/255.0f green:72.0f/255.0f blue:72.0f/255.0f alpha:1.0];
            break;
            
    }
    return rowColor;
}

@end