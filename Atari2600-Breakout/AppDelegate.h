//
//  AppDelegate.h
//  Atari2600-Breakout
//
//  Created by Thomas Donohue on 2/9/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

