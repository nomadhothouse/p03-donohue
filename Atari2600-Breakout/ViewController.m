//
//  ViewController.m
//  Atari2600-Breakout
//
//  Created by Thomas Donohue on 2/9/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize scoreLabel;
@synthesize livesLabel;
@synthesize gameField;

static BOOL initSubviews = NO;

- (void)viewDidLoad {
    
    [scoreLabel setFont:[UIFont fontWithName:@"Atari Font Full Version" size: scoreLabel.font.pointSize]];
    
    [livesLabel setFont:[UIFont fontWithName:@"Atari Font Full Version" size: livesLabel.font.pointSize]];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [gameField initField:scoreLabel label2: livesLabel];
}

- (void)viewDidLayoutSubviews {
    if(!initSubviews){
        [gameField initBricks];
    }
    initSubviews = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
