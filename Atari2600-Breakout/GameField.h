//
//  GameField.h
//  Atari2600-Breakout
//
//  Created by Thomas Donohue on 2/10/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface GameField : UIView
{
    CGRect bounds;
    CGFloat paddleX, paddleY;
    CGFloat paddleWidth;
    
    CGFloat dx, dy;
    
    NSMutableArray *brickArr;
    
    CGFloat oldX, oldY;
    BOOL dragging;
    
    CGFloat dragVelocity;
    
}
@property (nonatomic, assign) NSInteger score;
@property (nonatomic, assign) NSInteger lives;

@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) NSTimer *timer;

- (void) initField: (UILabel*) scoreLabel label2: (UILabel*) livesLabel;
- (void) initBricks;

@end

